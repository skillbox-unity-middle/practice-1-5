﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public partial class MovementSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = SystemAPI.Time.DeltaTime;

        foreach (var (transform, movement) in
                 SystemAPI.Query<RefRW<LocalToWorld>, RefRO<Movement>>())
        {
            transform.ValueRW.Value = float4x4.Translate(new float3(transform.ValueRW.Position.x + movement.ValueRO.speed * deltaTime, 0, 0));
        }
    }
}

