﻿using Unity.Entities;
using UnityEngine;

public class MovementAuthoring : MonoBehaviour
{
    public float speed;
}

public class MovementBaker : Baker<MovementAuthoring>
{
    public override void Bake(MovementAuthoring authoring)
    {
        TransformUsageFlags transformUsageFlags = new TransformUsageFlags();
        Entity entity = GetEntity(transformUsageFlags);
        AddComponent(entity, new Movement
        {
            speed = authoring.speed
        });
    }
}